# frozen_string_literal: true

require 'rmodbus'
require 'pry' if ENV['RUBY_ENV'] == 'development'
require_relative 'pzem016/version'
require_relative 'pzem016/pzem016'

module Pzem016
  class Error < StandardError; end
  # Your code goes here...
end
