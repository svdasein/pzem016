# frozen_string_literal: true

require_relative 'lib/pzem016/version'

Gem::Specification.new do |spec|
  spec.name = 'pzem016'
  spec.version = Pzem016::VERSION
  spec.authors = ['Dave Parker']
  spec.email = ['dparker@svdasein.org']

  spec.summary = 'Library and cli utils to interact with PZEM-016 power monitoring units'
  spec.description = 'PZEM-016 is a cheap power monitoring thing you can get from aliexpress for usually very little money https://www.aliexpress.com/w/wholesale-pzem%2525252d016.html?spm=a2g0o.best.search.0.  This gem provides a library to talk to them with and some cli utils'
  spec.homepage = "https://gitlab.com/svdasein/pzem016"
  spec.required_ruby_version = '>= 3.0.0'

  spec.metadata["allowed_push_host"] = "https://rubygems.org"

  # spec.metadata["homepage_uri"] = spec.homepage
  spec.metadata["source_code_uri"] = "https://gitlab.com/svdasein/pzem016"
  spec.metadata["changelog_uri"] = "https://gitlab.com/svdasein/pzem016/-/blob/master/CHANGELOG"

  spec.files = Dir.chdir(__dir__) do
    `git ls-files -z`.split("\x0").reject do |f|
      (File.expand_path(f) == __FILE__) ||
        f.start_with?(*%w[bin/ test/ spec/ features/ .git appveyor Gemfile])
    end
  end
  spec.bindir = 'exe'
  spec.executables = spec.files.grep(%r{\Aexe/}) { |f| File.basename(f) }
  spec.require_paths = ['lib']

  spec.add_dependency 'awesome_print'
  spec.add_dependency 'optimist'
  spec.add_dependency 'rmodbus'
  spec.add_dependency 'ruby-progressbar'
  spec.add_dependency 'sinatra'
  spec.add_dependency 'rackup'
end
