# Pzem016

[![Gem Version](https://badge.fury.io/rb/pzem016.svg)](https://badge.fury.io/rb/pzem016)

Pzem016 is some Ruby support for some very cheap but decent power monitors you can get from AliExpress

These things: https://www.aliexpress.com/w/wholesale-pzem%2525252d016.html?spm=a2g0o.best.search.0

This gem provides a library to wrap some of the modbus details, and a cli so you can interact
with them right away without having to write code.

You'll need an RS-485 adapter of some sort - I have had good luck with https://www.amazon.com/dp/B076WVFXN8?ref=ppx_yo2ov_dt_b_product_details&th=1

Wiring is very simple.  For a single point to point it's just A<->A, B<->B, GND<->GND.  You can have more than one node on the bus. If you intend
to do that you'll need to look at doing some impedance matching with terminating resistors at both ends.

![RS-485 wiring](PZEM016-bench.jpg)

## Installation

Install the gem and add to the application's Gemfile by executing:

    $ bundle add pzem016

If bundler is not being used to manage dependencies, install the gem by executing:

    $ gem install pzem016

## Usage

Library documentation is here: [TODO]

Example of output:
![sample usage](sampleoutput.png)

The command line tool's cli syntax uses subcommands:

```
pzem016.rb --help
PZEM-016 power measurement module utility.

Syntax: pzem016.rb [global options] <command> [options]

Commands: ["read", "readconfig", "setconfig", "resetenergy", "scanbus"]

Global options:
  -p, --port=<s>       Serial port (default: /dev/ttyUSB0)
  -s, --speed=<i>      Port speed (default: 9600)
  -t, --timeout=<i>    Retry timeout (default: 1)
  -r, --retries=<i>    Number of times to retry ModBus command (default: 1)
  -n, --name=<s>       Device name (default: PZEM-016 Device)
  -d, --debug          Extra noise
  -h, --help           Show this message
```

```
pzem016.rb read --help
Read power values from specified device
  -s, --sladdr=<i>    Slave address (default: 1)
  -j, --json          Output JSON
  -h, --help          Show this message
```

```
pzem016.rb readconfig --help
Read config values from specified device
  -s, --sladdr=<i>    Slave address (default: 1)
  -j, --json          Output JSON
  -h, --help          Show this message
```

```
pzem016.rb setconfig --help
Set device address and/or alarm threshold: pzem016.rb setconfig <addr|alarm> <value>
  -s, --sladdr=<i>    Slave address (default: 1)
  -h, --help          Show this message
```

```
pzem016.rb resetenergy  --help
Reset (clear) the energy counter (KWH) on the specified device
  -s, --sladdr=<i>    Slave address (default: 1)
  -h, --help          Show this message
```

```
pzem016.rb scanbus  --help
Scan ModBus for nodes/addrs
  -r, --read    Try to read values when node is found
  -h, --help    Show this message
```


## Development

After checking out the repo, run `bin/setup` to install dependencies. You can also run `bin/console` for an interactive prompt that will allow you to experiment.

To install this gem onto your local machine, run `bundle exec rake install`. To release a new version, update the version number in `version.rb`, and then run `bundle exec rake release`, which will create a git tag for the version, push git commits and the created tag, and push the `.gem` file to [rubygems.org](https://rubygems.org).

## Contributing

Bug reports and pull requests are welcome on GitLab at https://gitlab.com/svdasein/pzem016
