#!/usr/bin/env ruby
require 'sinatra/base'
require 'pry'

class PromServer < Sinatra::Base
         get '/metrics' do
            content_type 'text/plain'
            data = JSON.parse(%x(./pzem016.rb read --json))
            data.delete('DeviceName')
            data.delete('Alarm')
            puts data.map { |k, v| "#{k} #{v}" }.join("\n")
            data.map { |k, v| "#{k} #{v}" }.join("\n")
         end
end

PromServer.run!(port: 9110)
